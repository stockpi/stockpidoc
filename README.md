# StockPiDoc

Part of the [Stockpi project](https://gitlab.com/stockpi). 

Also see https://gitlab.com/stockpi/stockpidoc/-/wikis/home

## Installation
To install the documentation: Use git pull or download individual files

## Usage
General documentation of the Stockpi project.

## Roadmap
Must become the Stockpi documentation for the Stockpi Lib, the command line app and the Desktop app in Linux

## Contributing
Currently we werk together on this project in the radioshow webgang on Radio Centraal, any help welcome.

## Authors and acknowledgment
Wim.webgang, Marthe, Silvia

## License
GPL v3

## Project status
Active (starting up)
